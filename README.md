# Documentación en un proyecto node

## 1 - Correr la aplicación
```
>$ npm install

```

Después:
```
>$ npm start
```

## 2 - Agregar documentación

Una vez instalado el plugin, [DocumentThis](https://marketplace.visualstudio.com/items?itemName=joelday.docthis), oprimir ctrl + alt + d dos veces para que la herramienta genere la documentación automáticamente.

## 3 - Revisar coverage de documentación

Instalar [InchJS](https://github.com/rrrene/inchjs):

```
$ npm install -g inchjs
```

Ejecución para generar el reporte:
```
$ inchjs

```

Si por alguna razón, no te funciona de esta forma, intenta con los siguientes comandos:
```
sudo apt-get install ruby-full
```

Después: 
```
gem install inch
```

Para generar el reporte hacemos:
```
inchjs --pedantic
```

